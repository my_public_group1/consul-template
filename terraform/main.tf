module "aws-consul" {
  source = "./modules/iac"
  aws_region = "us-west-2"
  aws_keyname = "aws-west2"
  ssh_key_private = "/var/www/aws-west2.pem"
  grafana_user = "admin"
  grafana_pass = "Admin123!"
  pagerduty_user_url = "https://events.pagerduty.com/generic/2010-04-15/create_event.json"
  pagerduty_user_key = "77cf491e13774902d0cd03f0f64729de"
}
