resource "aws_instance" "consul_server" {
  count = 3
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.consul_server.id]
  key_name = "${var.aws_keyname}"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Consul Server ${count.index}"
  }

  provisioner "remote-exec" {
    inline = ["sleep 15s",
              "sudo apt-get update -y",
              "sleep 10s",
              "sudo apt-get install python3 -y",
              "sudo apt-get install python3-pip -y"]
    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      echo 'consul_server_ip: ${self.private_ip}' > '${var.def_path}/server_ip.yml'
#      ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_key_private} ../ansible/consul_servers/main.yml -b -v --extra-vars \
#        '{"consul_bind_address":"${self.private_ip}","consul_client_address":"${self.private_ip} 127.0.0.1","consul_node_role":"server","consul_bootstrap_expect":"true"}'
      echo 'serv${count.index} ansible_ssh_host=${self.public_ip} ansible_ssh_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private} consul_bind_address=${self.private_ip} consul_client_address="${self.private_ip} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true' >> ../ansible/serv.inv
    EOT
  }
}


resource "aws_instance" "consul_client" {
  count = 3
  # с выбранным образом
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.consul_client.id]
  key_name = "${var.aws_keyname}"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Consul Client ${count.index}"
  }

  provisioner "remote-exec" {
    inline = ["sleep 15s",
              "sudo apt-get update -y",
              "sleep 10s",
              "sudo apt-get install python3 -y",
              "sudo apt-get install python3-pip -y"]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
#      ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_key_private} ../ansible/consul_clients/main.yml -b -v --extra-vars \
#        '{"consul_bind_address":"${self.private_ip}","consul_client_address":"${self.private_ip} 127.0.0.1","consul_node_role":"client","consul_enable_local_script_checks":"true"}'
      echo 'client${count.index} ansible_ssh_host=${self.public_ip} ansible_ssh_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private} consul_bind_address=${self.private_ip} consul_client_address="${self.private_ip} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true' >> ../ansible/client.inv
    EOT
  }
}

resource "aws_instance" "mon_server" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.mon_server.id]
  key_name = "${var.aws_keyname}"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Monitoring Server"
  }

  provisioner "remote-exec" {
    inline = ["sleep 15s",
              "sudo apt-get update -y",
              "sleep 10s",
              "sudo apt-get install python3 -y",
              "sudo apt-get install python3-pip -y"]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      echo 'grafana_user: ${var.grafana_user}' >> '${var.def_path}/monitoring.yml'
      echo 'grafana_password: ${var.grafana_pass}' >> '${var.def_path}/monitoring.yml'
      echo 'pagerduty_user_url: ${var.pagerduty_user_url}' >> '${var.def_path}/monitoring.yml'
      echo 'pagerduty_user_key: ${var.pagerduty_user_key}' >> '${var.def_path}/monitoring.yml'
      ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_key_private} ../ansible/monitoring/main.yml -b -v
      rm -f '${var.def_path}/monitoring.yml'
    EOT
  }
}
