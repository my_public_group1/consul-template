variable "ssh_key_private" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "allowed_cidr_blocks" {
  type = list
  default = ["0.0.0.0/0"]
}

variable "server_tcp_ports" {
  type = list
  default = ["22", "8600", "8500", "8501", "8502", "8301", "8302", "8300"]
}

variable "server_udp_ports" {
  type = list
  default = ["8600", "8301", "8302"]
}

variable "client_tcp_ports" {
  type = list
  default = ["22", "80", "443", "8600", "8500", "8501", "8502", "8301", "8302", "8300", "9100"]
}

variable "client_udp_ports" {
  type = list
  default = ["8600", "8301", "8302"]
}

variable "mon_ports_udp" {
  type = list
  default = ["9094"]
}

variable "mon_ports_tcp" {
  type = list
  default = ["9090", "3000", "9093", "22", "9100", "9094", "80", "8428"]
}


variable "grafana_user" {
  type = string
}

variable "grafana_pass" {
  type = string
}

variable "aws_keyname" {
  type = string
}

variable "def_path" {
  type = string
  default = "../ansible/defaults"
}

variable "pagerduty_user_url" {
  type = string
}

variable "pagerduty_user_key" {
  type = string
}
