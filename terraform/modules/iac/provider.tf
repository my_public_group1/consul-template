# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = "${var.aws_region}"
}
