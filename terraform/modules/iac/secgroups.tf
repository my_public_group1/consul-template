resource "aws_security_group" "consul_server" {
  name = "Dynamic Security Group for Consul Servers"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${var.server_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

 dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${var.server_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for Consul Servers"
  }
}

resource "aws_security_group" "consul_client" {
  name = "Dynamic Security Group for Consul Clients"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${var.client_tcp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

 dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${var.client_udp_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for Consul Clients"
  }
}

resource "aws_security_group" "mon_server" {
  name        = "Monitoring Security Group"

  dynamic "ingress" {
    for_each = "${var.mon_ports_tcp}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    for_each = "${var.mon_ports_udp}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Monitoring Server Security Group"
  }
}
